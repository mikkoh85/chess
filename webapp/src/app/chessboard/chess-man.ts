import { LoopBackConfig } from '../shared/sdk';

export class ChessmenInit {
    public chessMen: ChessMan[] = [];

    constructor() {
        LoopBackConfig.setBaseURL('http://52.174.198.187');
        LoopBackConfig.setApiVersion('server/api');
        this.initKings();
        this.initQueens();
        this.initRooks();
        this.initBishops();
        this.initKnights();
        this.initPawns();
    }

    initKings() {
        //Init King Model
        let kingModel: ChessManModel = {
            imgUrls: {
                black: "/Chess_kdt45.svg",
                white: "/Chess_klt45.svg"
            },
            startingPositions: {
                blacks: [{x: 5, y: 1}],
                whites: [{x: 4, y: 8}]
            },
            type: ChessManType.King,
            possibleMoves: [
                {x: 1, y: 1},
                {x: 1, y: 0},
                {x: 1, y: -1},
                {x: 0, y: 1},
                {x: 0, y: -1},
                {x: -1, y: 1},
                {x: -1, y: 0},
                {x: -1, y: -1}
            ]
        };

        //Init Kings
        let blackKing = this.createChessMan(kingModel, 0, Color.Black);
        let whiteKing = this.createChessMan(kingModel, 0, Color.White);

        this.chessMen.push(blackKing);
        this.chessMen.push(whiteKing);
    }

    initQueens() {
        //Init Queen's moves
        let moves: Position[] = [];
        moves.push(...this.moveGen(1,1,7)); //UpLeft
        moves.push(...this.moveGen(1,0,7)); //Up
        moves.push(...this.moveGen(1,-1,7)); //UpRight
        moves.push(...this.moveGen(0,1,7)); //Left
        moves.push(...this.moveGen(0,-1,7)); //Right
        moves.push(...this.moveGen(-1,1,7)); //DownLeft
        moves.push(...this.moveGen(-1,0,7)); //Down
        moves.push(...this.moveGen(-1,-1,7)); //DownRight

        //Init Queen Model
        let queenModel: ChessManModel = {
            imgUrls: {
                black: "/Chess_qdt45.svg",
                white: "/Chess_qlt45.svg"
            },
            startingPositions: {
                blacks: [{x: 4, y: 1}],
                whites: [{x: 5, y: 8}]
            },
            type: ChessManType.Queen,
            possibleMoves: moves
        };

        //Init Queens
        let blackQueen = this.createChessMan(queenModel, 0, Color.Black);
        let whiteQueen = this.createChessMan(queenModel, 0, Color.White);

        this.chessMen.push(blackQueen);
        this.chessMen.push(whiteQueen);
    }

    initRooks() {
        //Init Rook's moves
        let moves: Position[] = [];
        moves.push(...this.moveGen(1,0,7)); //Up
        moves.push(...this.moveGen(0,1,7)); //Left
        moves.push(...this.moveGen(0,-1,7)); //Right
        moves.push(...this.moveGen(-1,0,7)); //Down

        //Init Rook Model
        let rookModel: ChessManModel = {
            imgUrls: {
                black: "/Chess_rdt45.svg",
                white: "/Chess_rlt45.svg"
            },
            startingPositions: {
                blacks: [{x: 1, y: 1}, {x: 8, y: 1}],
                whites: [{x: 1, y: 8}, {x: 8, y: 8}]
            },
            type: ChessManType.Rook,
            possibleMoves: moves
        };

        //Init Rooks
        this.chessMen.push(...[
            this.createChessMan(rookModel, 0, Color.Black),
            this.createChessMan(rookModel, 1, Color.Black),
            this.createChessMan(rookModel, 0, Color.White),
            this.createChessMan(rookModel, 1, Color.White)]);   
    }

    initBishops() {
        //Init Bishop's moves
        let moves: Position[] = [];
        moves.push(...this.moveGen(1,1,7)); //UpLeft
        moves.push(...this.moveGen(1,-1,7)); //UpRight
        moves.push(...this.moveGen(-1,1,7)); //DownLeft
        moves.push(...this.moveGen(-1,-1,7)); //DownRight

        //Init Queen Model
        let bishopModel: ChessManModel = {
            imgUrls: {
                black: "/Chess_bdt45.svg",
                white: "/Chess_blt45.svg"
            },
            startingPositions: {
                blacks: [{x: 3, y: 1}, {x: 6, y: 1}],
                whites: [{x: 3, y: 8}, {x: 6, y: 8}]
            },
            type: ChessManType.Bishop,
            possibleMoves: moves
        };

        //Init Bishops
        this.chessMen.push(...[
            this.createChessMan(bishopModel, 0, Color.Black),
            this.createChessMan(bishopModel, 1, Color.Black),
            this.createChessMan(bishopModel, 0, Color.White),
            this.createChessMan(bishopModel, 1, Color.White)]);  
    }

    initKnights() {
        //Init Knight Model
        let knightModel: ChessManModel = {
            imgUrls: {
                black: "/Chess_ndt45.svg",
                white: "/Chess_nlt45.svg"
            },
            startingPositions: {
                blacks: [{x: 2, y: 1}, {x: 7, y: 1}],
                whites: [{x: 2, y: 8}, {x: 7, y: 8}]
            },
            type: ChessManType.Knight,
            possibleMoves: [
                {x: 1, y: 2},
                {x: 1, y: -2},
                {x: -1, y: 2},
                {x: -1, y: -2},
                {x: 2, y: 1},
                {x: 2, y: -1},
                {x: -2, y: 1},
                {x: -2, y: -1}
            ]
        };

        //Init Knights
        this.chessMen.push(...[
            this.createChessMan(knightModel, 0, Color.Black),
            this.createChessMan(knightModel, 1, Color.Black),
            this.createChessMan(knightModel, 0, Color.White),
            this.createChessMan(knightModel, 1, Color.White)]); 
    }

    initPawns() {
        //Init Pawn Model
        let pawnModel: ChessManModel = {
            imgUrls: {
                black: "/Chess_pdt45.svg",
                white: "/Chess_plt45.svg"
            },
            startingPositions: {
                blacks: [{x: 1, y: 2}, {x: 2, y: 2}, {x: 3, y: 2}, {x: 4, y: 2}, {x: 5, y: 2}, {x: 6, y: 2}, {x: 7, y: 2}, {x: 8, y: 2}],
                whites: [{x: 1, y: 7}, {x: 2, y: 7}, {x: 3, y: 7}, {x: 4, y: 7}, {x: 5, y: 7}, {x: 6, y: 7}, {x: 7, y: 7}, {x: 8, y: 7}]
            },
            type: ChessManType.Pawn,
            possibleMoves: [
                {x: 0, y: 1},
                {x: 0, y: 2}
            ],
            eatingMoves: [
                {x: 1, y: 1},
                {x: -1, y: 1}
            ]
        };

        //Init Pawns
        for(let i=0;i<8;i++) {
            this.chessMen.push(
                ...[this.createChessMan(pawnModel, i, Color.Black),
                this.createChessMan(pawnModel, i, Color.White)]);
        } 
    }

    parseNgStyleString(imgUrl: string) {
        let path = LoopBackConfig.getPath();
        let serverPath = "/server";
        let styleString = {
            "background-image":
        "url('" + path + 
        serverPath + imgUrl + "')", 
        "background-size": "cover"};
        console.log(styleString);
        return styleString;
    }

    moveGen(x: number, y: number, times: number) {
        let movesArray: Position[] = [];
        for(let i=1; i<=times; i++) {
            let move: Position = {x: i*x, y: i*y};
            movesArray.push(move);
        }
        return movesArray;
    }

    createChessMan(model: ChessManModel, orderNumber: number, color: Color) {
        let name = Color[color] + " " + ChessManType[model.type];
        let style: any;
        let position: Position;

        if (color == Color.White){
            style = this.parseNgStyleString(model.imgUrls.white);
            position = model.startingPositions.whites[orderNumber];
        } else {
            style = this.parseNgStyleString(model.imgUrls.black);
            position = model.startingPositions.blacks[orderNumber];
        }
 
        let chessMan: ChessMan = {
            color: color,
            eatingMoves: model.eatingMoves ? model.eatingMoves : model.possibleMoves,
            style: style,
            name: name,
            position: position,
            orderNumber: orderNumber,
            possibleMoves: model.possibleMoves,
            type: model.type,
            firstMoveDone: false
        };

        if (chessMan.type == ChessManType.Pawn) {
            if (color == Color.Black) {
                chessMan.direction = Direction.Downwards;
            } else {
                chessMan.direction = Direction.Upwards;
            }
        }

        return chessMan;
    }
}

export interface ChessManModel {
    startingPositions: {
        whites: Position[], 
        blacks: Position[]
    };
    type: ChessManType;
    possibleMoves: Position[];
    eatingMoves?: Position[];
    imgUrls: ImageUrls;
}

export interface ChessMan {
    name: string;
    orderNumber: number;
    position: Position;
    type: ChessManType;
    possibleMoves: Position[];
    direction?: Direction;
    firstMoveDone?: Boolean;
    eatingMoves: Position[];
    color: Color;
    style: any;
    eaten?: Boolean;

}

export interface ImageUrls {
    white: string;
    black: string;
}  

export interface Position {
    x: number;
    y: number;
}

export enum Direction {
    Upwards = -1,
    Downwards = 1
}

export enum Color {
    Black = 1,
    White = 2
}

export enum ChessManType {
    King = 0,
    Queen = 1,
    Rook = 2,
    Bishop = 3,
    Knight = 4,
    Pawn = 5
}