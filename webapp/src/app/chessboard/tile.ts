import { ChessMan, Position } from './chess-man';

export class Tile {
    public position: Position;
    public class: string;
    public style: object;
    public chessMan: ChessMan;
}

export enum TileColumn {
    a = 1,
    b = 2,
    c = 3,
    d = 4,
    e = 5, 
    f = 6,
    g = 7,
    h = 8
}
