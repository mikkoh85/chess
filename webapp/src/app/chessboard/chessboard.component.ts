import { Component, HostListener, OnInit } from '@angular/core';
import { Tile, TileColumn } from './tile';
import { ChessMan, ChessManType, Color, ChessmenInit, Position, Direction } from './chess-man';
import { SessionInterface, Session, FireLoopRef } from '../shared/sdk/models';
import { RealTime } from '../shared/sdk/services';
import { LoopBackConfig } from '../shared/sdk';

@Component({
  selector: 'app-chessboard',
  templateUrl: './chessboard.component.html',
  styleUrls: ['./chessboard.component.scss']
})
export class ChessboardComponent implements OnInit {
  game: Boolean;
  ratio = '1:1';
  tiles: Tile[] = [];
  chessmen: ChessMan[] = [];
  selected: ChessMan;
  country: Color = Color.Black;
  status: string;
  turn: Boolean;
  name: string = "Nick";
  opponent: string;
  joined: Boolean;
  winStatus: string;

  session: Session;
  sessions: Session[] = new Array<Session>();
  sessionRef : FireLoopRef<Session>;

  constructor(private rt: RealTime) {
        LoopBackConfig.setBaseURL('http://52.174.198.187');
        LoopBackConfig.setApiVersion('server/api');
    this.rt.onReady().subscribe(() => {
      this.sessionRef = this.rt.FireLoop.ref<Session>(Session);
      this.sessionRef.on('change').subscribe((sessions: Session[]) => {
        this.sessions = sessions;
        if (!this.session) return;
        let onGoing = this.sessions.find(session => 
        session.host == this.session.host 
        && session.createdAt == this.session.createdAt);


        if (!onGoing && !this.winStatus) {
          this.hostSession(false);
          this.status = "Game ended. " + this.opponent + " left.";
          this.removeSession(this.session);
        }
        
        if(onGoing.winStatus) {
          this.winStatus = onGoing.winStatus;
          this.status = this.winStatus + " Game ended.";
          this.removeSession(this.session);
          return;
        }

        if (onGoing.opponent && !this.session.opponent) {
          console.log("Opponent added!!");
          this.session.opponent = this.opponent = onGoing.opponent;
        }

        this.session = onGoing;
        this.chessmen = onGoing.chessmen;
        this.status = onGoing.status;
        this.upDateBoard(this.chessmen); 
        //Add turn
        if (this.joined && onGoing.turn == "opponent") {
          this.turn = true;
        } 
        if (this.joined && onGoing.turn == "host") {
          this.turn = false;
        }
        if (!this.joined && onGoing.turn == "opponent") {
          this.turn = false;
        }
        if (!this.joined && onGoing.turn == "host") {
          this.turn = true;
        }
      });
      // this.sessionRef.stats().subscribe((stats: any) => console.log("stats:", stats));
    });
    this.rt.onDisconnect().subscribe((error: string) => {
      console.log('Disconnected', error);
    });
    this.rt.onUnAuthorized().subscribe((error: string) => {
      console.log('UnAuthorized', error);
    });
    this.rt.onAuthenticated().subscribe((error: string) => {
      console.log('Authenticated', error);
    });
  }

  createSession(): void {
    let chessmen = new ChessmenInit().chessMen;
    let session = new Session();
    session.host = this.name;
    session.sessionName = this.name + "'s game";
    session.hostsCountry = this.country;
    session.status = this.status;
    if (this.turn) session.turn = "host";
    else session.turn = "opponent";
    session.chessmen = chessmen;

    this.sessionRef.create(session).subscribe(created => {
      this.chessmen = created.chessmen;
      this.upDateBoard(this.chessmen);
      this.session = created;
    });
  }

  updateSession(session: Session): void {
    this.sessionRef.upsert(session).subscribe();
  }

  removeSession(session: Session): void {
    if (session)
    this.sessionRef.remove(session).subscribe(
      result => {
        console.log("Remove result:", result);
        this.session = null;
        // this.rt.IO.emit('disconnect', this.name);
      }, err => {
        console.log("Removing error:", err);
        this.session = null;
      }
    );
  }

  hostSession(session: Boolean) {
    if(session) {
      this.winStatus = null;
      this.game = true;
      this.status = "Please wait another player to join.";
      this.initTiles();
      // this.initChessMen();
      if (this.country == Color.White)
      this.turn = true;
      else this.turn = false;

      this.createSession();
    } else {
      // this.country = null;
      this.status = "Game ended."
      this.removeSession(this.session);
    }
  }

  joinSession(session: Session) {
    if (this.name !== session.host)
    session.opponent = this.name;
    else {
      alert("You have to have different name than host!");
      return;
    }
    this.winStatus = null;
    let joinAction = true;
    this.joined = true;
    if (session.hostsCountry == Color.Black) {
      this.country = Color.White;
      this.turn = true;
    }
    else {
      this.country = Color.Black;
      this.turn = false;
    }
    this.updateSession(session);
    this.game = true;
    this.status = "Game started!"
    this.initTiles();
    this.opponent = session.host;
    this.sessionRef.upsert(session).subscribe(
      updated => {
        if(joinAction) {
          this.chessmen = updated.chessmen;
          this.upDateBoard(this.chessmen);
          this.session = updated;
        }
        joinAction = false;
      }
    );
  }

  countryButton(color: string) {
    if(color == "white" && this.country == Color.White)
    return {"background": "burlywood"}; 
    else if(color == "black" && this.country == Color.Black)
    return {"background": "burlywood"};
    else return {};
  }

  setCountry(color: string) {
    if (color == "white") this.country = Color.White;
    if (color == "black") this.country = Color.Black;
  }

  endGame(){
    this.hostSession(false);
    this.game = false;
    this.tiles = [];
    this.chessmen = [];
    this.selected = null;
    this.country = Color.Black;
    this.turn = false;
    this.opponent = null;
    this.joined = false;
    this.winStatus = null;
  }

  ngOnInit() {
    this.initTiles();
    if (!this.session) this.status = "Waiting a session to be started";
  }

  @HostListener('window:unload', ['$event'])
  unloadHandler(event: any) {
    if (this.session) this.removeSession(this.session);
  }

  initTiles() {
    this.tiles = [];
    for (let row = 1; row <= 8; row++) {
      for(let column: TileColumn = TileColumn.a; column <= TileColumn.h; column++) {
        let tile = new Tile();
        tile.position = {x: column, y: row}
        tile.class = TileColumn[column] + row;
        this.tiles.push(tile);
      }
    }
  }

  initOpponent(name: string) {
    this.opponent = name;
    this.session.opponent = name;
    this.updateSession(this.session);
    this.status = "Game started!";
  }

  upDateBoard(chessMen: ChessMan[]) {
    this.tiles.forEach(tile => {
      tile.chessMan = null;
    });
    chessMen.forEach(chessMan => {
      if (!chessMan.eaten)
      this.tiles.find(tile => chessMan.position.x == tile.position.x && chessMan.position.y == tile.position.y).chessMan = chessMan;
    });
  }

  onClick(tile: Tile) {
    if (!this.session || !this.turn || !this.opponent) return;
    // console.log("clicked", TileColumn[tile.position.x], tile.position.y);
    // let tiles = this.tiles.filter(found => found.chessMan == this.chessmen[0])
    // tiles.forEach(one => {
    //   one.chessMan = null;
    // });
    // this.tiles.find(found => found == tile).chessMan = this.chessmen[0];

    //Selecting own ChessMan
    if (tile.chessMan && tile.chessMan.color == this.country) {
      this.selectChessMan(tile);
      this.updateSession(this.session);
    }

    //Moving own ChessMan
    else if (!tile.chessMan && this.selected) {
      this.moveChessMan(tile);
      this.updateSession(this.session);
    }

    //Eating opponent's ChessMan
    else if (tile.chessMan && tile.chessMan.color != this.country) {
      this.eatChessMan(tile);
      this.updateSession(this.session);
    }

    else this.status = null;
  }

  selectChessMan(tile: Tile) {
    this.selected=tile.chessMan; 
    this.status = this.name + " selected: " + this.selected.name;
    this.session.status = this.status;
  }

  moveChessMan(tile: Tile) {
    let man = this.selected;

    let possibleMoves: Position[]; 
    possibleMoves = this.updateMoves(man.possibleMoves);
    if (man.firstMoveDone) {
      possibleMoves.splice(1);
    }

    //If clicked tile matches with possible moves, move
    if(possibleMoves.find(move => 
      move.x == tile.position.x 
      && move.y == tile.position.y) 
      && !this.foundCollisions(tile)) {      
        this.status = this.name + " moved " + man.name + " to " + TileColumn[tile.position.x] + tile.position.y + ".";
        this.session.status = this.status;

        this.tiles.find(tile => 
          tile.position.x == man.position.x 
          && tile.position.y == man.position.y)
          .chessMan = null;
        man.position.x = tile.position.x;
        man.position.y = tile.position.y
        tile.chessMan = man;
        this.pawnMoveRules();
        this.chessmen.find(chessman => chessman.name == man.name && chessman.orderNumber == man.orderNumber).position = man.position;
        this.session.chessmen = this.chessmen;
        if(!this.joined) this.session.turn = "opponent";
        else this.session.turn = "host";

        //Deselect the selected
        this.selected = null;
      } else this.status = "Not possible move, " + this.name + ". Try again.";
  }

  eatChessMan(tile: Tile) {
    let man = this.selected;

    let eatingMoves: Position[]; 
    eatingMoves = this.updateMoves(man.eatingMoves);

    //If clicked tile matches with possible moves, move
    if(eatingMoves.find(move => 
      move.x == tile.position.x 
      && move.y == tile.position.y) 
      && !this.foundCollisions(tile)) { 
        tile.chessMan.eaten = true; 
        this.status = 
          this.name + 
          " ate " + 
          tile.chessMan.name + 
          " with " + 
          man.name + ".";
        this.session.status = this.status;

        this.tiles.find(tile => 
          tile.position.x == man.position.x 
          && tile.position.y == man.position.y)
          .chessMan = null;
        man.position.x = tile.position.x;
        man.position.y = tile.position.y
        tile.chessMan = man;
        this.chessmen.find(chessman => chessman.name == man.name && chessman.orderNumber == man.orderNumber).position = man.position;
        this.pawnMoveRules();
        this.session.chessmen = this.chessmen;
        if(!this.joined) this.session.turn = "opponent";
        else this.session.turn = "host";

        //Deselect the selected
        this.selected = null;

        //If King is eaten, game ends
        let eatenKing = this.chessmen.find(chessman => 
        chessman.type == ChessManType.King &&
        chessman.eaten == true)
        if (eatenKing) {
          if (eatenKing.color == this.country)
          this.winStatus = "Checkmate! " + this.opponent + " won.";
          else this.winStatus = "Checkmate! " + this.name + " won.";
          this.status = this.winStatus;
          this.session.winStatus = this.winStatus;
          // this.hostSession(false);
        }
        
      } else this.status = "Not possible move, " + this.name + ". Try again.";
  }

  updateMoves(movesModel: Position[]) {
    let man = this.selected;
    let moves: Position[] = [];
    moves = JSON.parse(JSON.stringify(movesModel));
    moves.forEach(move => {
      if(man.direction) move.y *= man.direction;

      move.x += man.position.x;
      move.y += man.position.y;
    });
    return moves;
  }

  pawnMoveRules() {
    let man = this.selected;
    if(man.type == ChessManType.Pawn) {
      //Let's cut the pawn's speed after 1st run
      man.firstMoveDone = true;
      //TODO: Change pawn's direction when hit to wall
      if (man.position.y == 8 || man.position.y == 1) {
        console.log("Pawn Hit the wall!");
        if (man.direction == Direction.Downwards) 
          man.direction = Direction.Upwards;
        else man.direction = Direction.Downwards
      }
    }
    let theman = this.chessmen.find(chessman => chessman.name == man.name && chessman.orderNumber == man.orderNumber);
    theman.firstMoveDone = man.firstMoveDone;
    theman.direction = man.direction;
  }

  foundCollisions(tile: Tile) {
    let man = this.selected;
    if(man.type == ChessManType.Knight) return false;
    let positionsToCheck: Position[] = [];
    
    let x = tile.position.x - man.position.x;
    let y = tile.position.y - man.position.y;

    let count = Math.abs(x) >= Math.abs(y) ? Math.abs(x): Math.abs(y);

    for (let i = 1; i<count; i++) {
      let pos: Position = {
        x: man.position.x + (i*Math.sign(x)),
        y: man.position.y + (i*Math.sign(y))
      };
      positionsToCheck.push(pos);
    }
    let collisions =
    this.chessmen.filter(chessman => 
      positionsToCheck
      .find(position => 
        chessman.position.x == position.x &&
        chessman.position.y == position.y &&
        !chessman.eaten));
    if(collisions.length) console.log("collisions:", collisions);

    if (collisions.length) return true; else return false;
  }
}
