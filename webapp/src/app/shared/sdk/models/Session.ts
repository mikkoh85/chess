/* tslint:disable */

declare var Object: any;
export interface SessionInterface {
  "host": string;
  "sessionName": string;
  "hostsCountry"?: number;
  "opponent"?: string;
  "password"?: string;
  "turn"?: string;
  "status"?: string;
  "winStatus"?: string;
  "chessmen"?: any;
  "id"?: number;
  "createdAt"?: Date;
  "updatedAt"?: Date;
}

export class Session implements SessionInterface {
  "host": string;
  "sessionName": string;
  "hostsCountry": number;
  "opponent": string;
  "password": string;
  "turn": string;
  "status": string;
  "winStatus": string;
  "chessmen": any;
  "id": number;
  "createdAt": Date;
  "updatedAt": Date;
  constructor(data?: SessionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Session`.
   */
  public static getModelName() {
    return "Session";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Session for dynamic purposes.
  **/
  public static factory(data: SessionInterface): Session{
    return new Session(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Session',
      plural: 'Sessions',
      properties: {
        "host": {
          name: 'host',
          type: 'string'
        },
        "sessionName": {
          name: 'sessionName',
          type: 'string'
        },
        "hostsCountry": {
          name: 'hostsCountry',
          type: 'number'
        },
        "opponent": {
          name: 'opponent',
          type: 'string'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
        "turn": {
          name: 'turn',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "winStatus": {
          name: 'winStatus',
          type: 'string'
        },
        "chessmen": {
          name: 'chessmen',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date'
        },
        "updatedAt": {
          name: 'updatedAt',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
