# Chess MultiPlayer game

This is a HTML5 two player game made with:
* FireLoop
* Angular CLI

## Prerequisites

* NodeJS + NPM installed
* FireLoop installed
* Angular CLI installed

## First run

Clone or download the project and type to terminal at the root:  
`fireloop serve` then hit `a` and `enter` (starts both projects on the folder)  
..if that gives any error, try installing the packages on both projects:

1. `cd fireloop` go to fireloop folder
2. `npm i` install packages
3. `cd ..` back to root
4. `cd webapp` go to webapp folder
5. `npm i` install packages
6. `cd ..` go back to root
7. `fireloop serve` try serving again.

When server is running just head to [localhost:4200](http://localhost:4200/).

## Demo

You can try this game running online [here](http://52.174.198.187/).