var should    = require('chai').should();
var supertest = require('supertest');
var api       = supertest('http://localhost:3000/api');

describe('Session unit tests:', () => {
    it('Should create a Session instance', (done: Function) => {
        api.post('/sessions').send({
            host: 'test',
            hostsCountry: 12345,
            client: 'test',
            password: 'test',
            turn: 'test'
        }).expect(200, done);
    });
});
